from odoo import models, fields


PROVIDER =[
    ('institution', 'Institution'),
    ('corporate', 'Corporate'),
    ('organization', 'Organization'),
    ('individuals', 'Individuals')
]
SERVICES = [
    ('psychiatric', 'Psychiatric Services'),
    ('rehabilitation', 'Alcohol & Drugs Rehabilitation Services'),
    ('mental_health_care', 'Mental Health Care& Treatment Facilities'),
    ('mediation', 'Mediation')
]


class LinkageReferralCenter(models.Model):
    _name = 'linkage.referral.center'

    category_of_Service_Provider = fields.Selection(PROVIDER)
    listed_Services = fields.Selection(SERVICES)
    contacts_id = fields.Many2one('res.partner')
    ico_contacts_person = fields.Char('')
    name = fields.Char('Name')
    postal_address = fields.Char('Postal Address')
    email = fields.Char('Email')
    website = fields.Char('Website')
    massager_name = fields.Char('Name')
    phone = fields.Char('Phone')
    massage_email = fields.Char('Email')
    location = fields.Char('Location')
    price = fields.Integer('Price')
    operation_time = fields.Datetime('Operation Time')
    service_description = fields.Text('Service Description')


