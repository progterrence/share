{
    'name': 'Linkage and Referral Center',
    'summary': 'Protocol for  ICOs Admission form for Institutions',
    'version': '1.0',
    'category': 'Custom',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'share_crm'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/linkage_referral_center.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
