{
    'name': 'CRM Share',
    'summary': 'crm_share',
    'version': '12.0.1',
    'category': 'Custom',
    'author': 'Sunflower IT',
    'website': 'https://sunflowerweb.nl',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'crm',
        'calendar',
        'contacts',
        'purchase',
        'crm_phonecall',
        'sale_customer_wallet',
    ],
    'data': [
        'views/menu.xml',
        'views/calendar_event_type.xml',
        'views/res_partner.xml',
        'views/calendar_event.xml',
        'views/crm_lead.xml',

    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
