
from odoo import models, fields


class CalendarEventType(models.Model):
    _inherit = 'calendar.event.type'

    code = fields.Char('Code', required=True)
    description = fields.Char('Description')

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'Name must be uniq!'),

        ('code_uniq', 'unique (code)',
         'Code must be uniq!'),
    ]

