
from odoo import models, fields


class CalenderEvent(models.Model):
    _inherit = 'calendar.event'

    type_id = fields.Many2one('calendar.event.type', "Type", required=True)

