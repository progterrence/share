import string
from random import random

from odoo import models, fields, api
from datetime import date

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

SALUTATION = [
    ('mr', 'Mr'),
    ('mrs', 'Mrs'),
    ('doctor', 'Doctor')
]
GENDER = [
    ('male', "Male"),
    ('female', 'Female'),
    ('other', 'Other')
]
LEVEL_OF_EDUCATION = [
    ('primary', 'Primary'),
    ('secondary', 'Secondary'),
    ('tertiary', 'Tertiary College'),
    ('university', 'University')
]
MARITAL_STATUS = [
    ('single', 'Single'),
    ('married', 'Married'),
    ('divorced', 'Divorced'),
    ('others', 'Others')
]


class ResPartner(models.Model):
    _inherit = 'res.partner'

    last_name = fields.Char('Last Name')
    number = fields.Char('Number')
    adult = fields.Boolean(string='Adult?')
    salutation = fields.Selection(SALUTATION, onchange='onchange_gender',
                                  default='mr')
    gender = fields.Selection(GENDER)
    parrent_id = fields.Many2one('res.partner', string="Parent ID")
    username = fields.Char('Username')
    dor = fields.Date('Date of Registration')
    email_address = fields.Char('')
    phone_no = fields.Char('Phone')
    dob = fields.Date('Date of Birth')
    near_major_town = fields.Char('Near Major Town of Residence')
    level_of_education = fields.Selection(LEVEL_OF_EDUCATION)
    learning_institution = fields.Char('Institution of Learning')
    profession = fields.Char('Profession')
    marital_status = fields.Selection(MARITAL_STATUS)
    paid_subscription = fields.One2many('paid.subscription.line', 'partner_id')
    age = fields.Integer(string="Age", readonly=True, compute="_compute_age")

    @api.depends("dob")
    def _compute_age(self):
        for record in self:
            age = 0
            if record.dob:
                age = relativedelta(fields.Date.today(),
                                    record.dob).years
            record.age = age

    @api.onchange('salutation')
    def onchange_gender(self):
        if self.salutation == 'mr':
            self.gender = 'male'
        elif self.salutation == 'mrs':
            self.gender = 'female'
        else:
            ''


    @api.model
    def _get_default_country(self):

        country = self.env['res.country'].search([('code', '=', 'KE')], limit=1)

        return country

    country_id = fields.Many2one('res.country', string='Country',
                                 default=_get_default_country)
    nationality_id = fields.Many2one('res.country',
                                     default=_get_default_country)
    country_of_residence_id = fields.Many2one('res.country',
                                              default=_get_default_country)


class PaidSubscription(models.Model):
    _name = 'paid.subscription.line'

    partner_id = fields.Many2one('res.partner')
    subscription_date = fields.Date('Date')
    amount = fields.Integer('Amount')
