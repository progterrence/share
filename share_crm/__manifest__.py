{
    'name': 'CRM Share',
    'summary': 'Bio Data',
    'version': '1.0',
    'category': 'Custom',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'contacts',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
