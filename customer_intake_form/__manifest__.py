{
    'name': 'Client Intake Form',
    'summary': '',
    'version': '1.0',
    'category': 'Custom',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'share_crm'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/client_intake_form.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
