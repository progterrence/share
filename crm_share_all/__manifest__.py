{
    'name': 'CRM Share',
    'summary': 'crm share all modules',
    'version': '1.0',
    'category': 'Custom',
    'author': 'Aron',
    'license': 'LGPL-3',
    'depends': [
        #odoo
        'base',
        'contacts',
        #share
        'share_crm',
        'customer_intake_form',
        'chat_therapy_center_intake_form',
        'linkage_and_referral_center',


    ],
    'data': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
