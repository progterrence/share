# -*- coding: utf-8 -*-
{
    'name': 'Season Global',
    'version': '12.0.0.0',
    'category': 'Company',
    'sequence': 1,
    'summary': 'Company Document',
    'author': 'Terence Nzaywa',
    'website': '',
    'depends': [
        'base',
        'web',
        'sale',
        'hr_payroll',
        'fleet',
        'odoo_transport_management',
        'purchase',
        'stock_picking_customer_ref',
        'stock_picking_line_sequence',
        'account_invoice_line_sequence',
        'product_sequence',
        'purchase_order_line_sequence',
        'sale_order_line_sequence'
    ],
    'data': [
        'data/change_bot_name.xml',
        'views/report_delivery.xml',
        'views/report_invoice.xml',
        'views/report_purchaseorder.xml',
        'views/report_saleorder.xml',
        'views/sale_order.xml',
        'views/report_payslip.xml',
        'views/fleet_vehicle.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}