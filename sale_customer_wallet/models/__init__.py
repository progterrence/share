# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from . import account_journal
from . import res_partner
from . import wallet_recharge
from . import sale_order
from . import sale_wallet_transaction

