# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import fields, models, api
from datetime import date, time, datetime


class WalletRecharge(models.TransientModel):
    _name = 'wallet.recharge'

    recharge_amount = fields.Float('Recharge Amount', required="True")
    journal_id = fields.Many2one('account.journal', 'Payment Journal',
                                 required="True")

    @api.multi
    def post(self):
        context = self._context
        active_ids = context.get('active_ids')
        account_payment_obj = self.env['account.payment']
        partner_wallet_id = self.env['res.partner'].browse(active_ids[0])
        wallet_transaction_obj = self.env['sale.wallet.transaction']
        date_now = datetime.strftime(datetime.now(), '%Y-%m-%d')

        vals = {
            'name': self.env['ir.sequence'].with_context(
                ir_sequence_date=date_now).next_by_code(
                'account.payment.customer.invoice'),
            'payment_type': "inbound",
            'amount': self.recharge_amount,
            'communication': "Wallet Recharge",
            'payment_date': datetime.now().date(),
            'journal_id': self.journal_id.id,
            'payment_method_id': 1,
            'partner_type': 'customer',
            'partner_id': partner_wallet_id.id,
        }
        payment_create = account_payment_obj.sudo().create(vals)
        payment_create.post()  # Confirm Account Payment
        value = {
            'wallet_type': 'credit',
            'reference': 'manual',
            'amount': self.recharge_amount,
            'partner_id': partner_wallet_id.id,
            'currency_id': partner_wallet_id.property_product_pricelist.currency_id.id,
            'payment_refer': payment_create.id,
            'status': 'done',
        }
        wallet_obj = wallet_transaction_obj.sudo().create(value)

        total_amount = partner_wallet_id.wallet_balance + self.recharge_amount

        partner_wallet_id.write({'wallet_balance': total_amount})