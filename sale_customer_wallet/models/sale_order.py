# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import fields, models, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    wallet_used = fields.Float('Wallet Amount Used')
    wallet_transaction_id = fields.Many2one('sale.wallet.transaction',
        'Wallet Transaction')

    @api.multi
    def check_wallet_details_from_payment(self):
        wallet_transaction_obj = self.env['sale.wallet.transaction']
        for order_id in self:
            sale_order_id = order_id
            for sale_wallet in sale_order_id.account_payment_ids:
                if sale_wallet.journal_id.wallet == True:
                    vals = {
                        'wallet_type': 'debit',
                        'partner_id': sale_order_id.partner_id.id,
                        'sale_order_id': order_id.id,
                        'reference': 'manual',
                        'amount': sale_wallet.amount,
                        'currency_id': sale_order_id.pricelist_id.currency_id.id,
                        'status': 'done'
                    }
                    wallet_create = wallet_transaction_obj.sudo().create(vals)
                    sale_order_id.write({
                        'wallet_used': sale_wallet.amount,
                        'wallet_transaction_id': wallet_create.id
                    })