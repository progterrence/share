# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import fields, models, api
from datetime import datetime


class SaleWalletTransaction(models.Model):
    _name = 'sale.wallet.transaction'
    _order = 'id desc'

    name = fields.Char('Name')
    wallet_type = fields.Selection([
        ('credit', 'Credit'),
        ('debit', 'Debit')
    ], string='Type', default='credit')
    partner_id = fields.Many2one('res.partner', 'Customer')
    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    # sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    # wallet_id = fields.Many2one('res.partner', 'Wallet')
    reference = fields.Selection([
        ('manual', 'Manual'),
        ('sale_order', 'Sale Order')
    ], string='Reference', default='manual')
    amount = fields.Float('Amount')
    currency_id = fields.Many2one('res.currency', 'Currency')
    status = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done')
    ], string='Status', readonly=True, default='draft')

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code(
            'sale.wallet.transaction') or 'New'
        res = super(SaleWalletTransaction, self).create(vals)
        return res

    @api.multi
    def wallet_recharge(self, partner_id, wallet, journal):
        self.ensure_one()
        wallet_transaction_obj = self.env['sale.wallet.transaction']
        partner = self.env['res.partner'].browse(partner_id['id'])
        vals = {
            'wallet_type': 'credit',
            'partner_id': partner.id,
            # 'payment_order_id' : order_id,
            'reference': 'manual',
            'amount': wallet,
            'currency_id': partner.property_product_pricelist.currency_id.id,
            'status': 'done'
        }
        wallet_create = wallet_transaction_obj.sudo().create(vals)
        account_payment_obj = self.env['account.payment']
        date_now = datetime.strftime(datetime.now(), '%Y-%m-%d')

        if journal == 'cash':
            cash_journal_ids = self.env['account.journal'].search(
                [('type', '=', 'cash')])
            if cash_journal_ids:
                journal = cash_journal_ids[0].id

        if journal == 'check':
            bank_journal_ids = self.env['account.journal'].search(
                [('type', '=', 'bank')])
            if bank_journal_ids:
                journal = bank_journal_ids[0].id

        if journal == 'bank':
            bank_journal_ids = self.env['account.journal'].search(
                [('type', '=', 'bank')])
            if bank_journal_ids:
                journal = bank_journal_ids[0].id

        values = {
            'name': self.env['ir.sequence'].with_context(
                ir_sequence_date=date_now).next_by_code(
                'account.payment.customer.invoice'),
            'payment_type': "inbound",
            'amount': wallet,
            'communication': "Wallet Recharge",
            'payment_date': datetime.now().date(),
            'journal_id': journal,
            'payment_method_id': 1,
            'partner_type': 'customer',
            'partner_id': partner.id,
        }
        payment_create = account_payment_obj.sudo().create(values)
        payment_create.post()  # Confirm Account Payment

        total_amount = partner.wallet_balance + float(wallet)  # Total Amount

        partner.write({'wallet_balance': total_amount})

        return True
