# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import fields, models


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    wallet = fields.Boolean(string='Wallet Journal')
