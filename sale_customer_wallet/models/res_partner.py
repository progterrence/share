# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)

from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    wallet_balance = fields.Float('Last Wallet Amount')
    wallet_balance_calc = fields.Float(
        compute='_compute_wallet_transaction_count',
        string='Wallet Balance'
    )
    wallet_transaction_count = fields.Integer(
        compute='_compute_wallet_transaction_count', string="Wallet")

    @api.multi
    def _compute_wallet_transaction_count(self):
        wallet_data = self.env['sale.wallet.transaction'].search([
            ('partner_id', 'in', self.ids)])
        debit = sum(wallet_data.filtered(
            lambda x: x.wallet_type == 'debit').mapped('amount'))
        credit = sum(wallet_data.filtered(
            lambda x: x.wallet_type == 'credit').mapped('amount'))
        for partner in self:
            partner.wallet_transaction_count = len(wallet_data)
            partner.wallet_balance_calc = credit - debit
