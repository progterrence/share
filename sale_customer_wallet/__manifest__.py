# Copyright 2019 Sunflower IT <https://www.sunflowerweb.nl>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl)
{
    'name':'Sale Customer Wallet',
    'category': 'Sales',
    'author': 'Sunflower IT',
    'website': 'https://sunflowerweb.nl',
    'summary': 'Process SO using customer wallet',
    'version': '12.0.1.0.0',
    'license': 'LGPL-3',
    'depends': [
        'sale_management',
        'account'
    ],
    'data': [
        'data/ir_sequence.xml',
        'security/ir.model.access.csv',
        'views/account_journal_view.xml',
        'views/sale_order_view.xml',
        'views/sale_order_transaction_view.xml',
        'views/wallet_recharge_view.xml',
        'views/res_partner_view.xml',
    ],
    'installable': True,
}
