{
    'name': 'Chat Therapy Center',
    'summary': 'Chat Therapy Center Intake form',
    'version': '1.0',
    'category': 'Custom',
    'license': 'LGPL-3',
    'depends': [
        'base',
        'share_crm'

    ],
    'data': [
        'security/ir.model.access.csv',
        #'views/chat_therapy_center.xml',
        'views/counselling_services_available.xml',
        'views/counsellor_session_summary_notes.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True
}
