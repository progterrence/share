from odoo import models, fields, api
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

SALUTATION = [
    ('mr', 'Mr'),
    ('mrs', 'Mrs'),
    ('doctor', 'Doctor')
]
GENDER = [
    ('male', "Male"),
    ('female', 'Female'),
    ('other', 'Other')
]
CGENDER = [
    ('male', "Male"),
    ('female', 'Female'),
    ('any', 'Any')
]
LANGPROF = [
    ('english', 'English'),
    ('kiswahili', 'Kiswahili'),
    ('others', 'Others')
]
LEVELOFEDUCATION = [
    ('primary', 'Primary'),
    ('secondary', 'Secondary'),
    ('tertiary', 'Tertiary College'),
    ('university', 'University')
]
EDULEVEL = [
    ('primary', 'Primary'),
    ('secondary', 'Secondary'),
    ('tertiary', 'Tertiary College'),
    ('university', 'University')
]

MARITALSTATUS = [
    ('single', 'Single'),
    ('married', 'Married'),
    ('divorced', 'Divorced'),
    ('others', 'Others')
]
KNOWHOW = [
    ('facebook', 'Facebook'),
    ('twitter', 'Twitter'),
    ('instagram', 'Instagram'),
    ('youtube', 'Youtube')
]
SESSIONPURPOSE = [
    ('promote_mental_wellness', 'Promote mental wellness'),
    ('requirement', 'A requirement by some authority.'),
    ('curiosity', 'Personal initiative out of curiosity.'),
    ('counselling_course_requirement ', 'Counselling course requirement '),
    ('personal_development', 'Personal development therapy Session.'),
    ('professional_requirement', 'Professional requirement to be accredited.'),
    ('academic_requirement', 'Academic requirement to complete course work'),
    ('supervision', 'Supervision'),
    ('manage_mental_health_conditions', 'Manage mental health conditions '),
    ('manage_medical_condition', 'Manage medical condition.'),
    ('others', 'Others....')

]
AGEGROUP = [
    ('21-30', '21- 30 Yrs.'),
    ('31-40', '31- 40 Yrs.'),
    ('41-50', '41- 50 Yrs.'),
    ('51-60', '51- 60 Yrs.'),
    ('61_and_above', '61 and above.'),
    ('open_to_any_age', 'Open to any age')
]


class ResPartner(models.Model):
    _name = 'chat.therapy.center'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Name')
    nature_of_account = fields.Char('Nature of Account')
    number = fields.Char('Number')
    salutation = fields.Selection(SALUTATION)
    gender = fields.Selection(GENDER)
    username = fields.Char('Username')
    dor = fields.Date('Date of Registration')
    nationality_id = fields.Many2one('res.country')
    country_of_residence_id = fields.Many2one('res.country')
    country_id = fields.Many2one('res.country')
    email_address = fields.Char('')
    phone_no = fields.Char('Phone')
    dob = fields.Date('Date of Birth')
    near_major_town = fields.Char('Near Major Town of Residence')
    level_of_education = fields.Selection(LEVELOFEDUCATION)
    learning_institution = fields.Char('Institution of Learning')
    profession = fields.Char('Profession')
    marital_status = fields.Selection(MARITALSTATUS)
    know_how = fields.Selection(KNOWHOW)
    session_purpose = fields.Selection(SESSIONPURPOSE)
    age_group = fields.Selection(AGEGROUP)
    c_gender = fields.Selection(CGENDER)
    education_level = fields.Selection(EDULEVEL)
    language_proficiency = fields.Selection(LANGPROF)
    availability = fields.Date('Availability')
    specific_counsellor = fields.Char('Counsellor User Name')

