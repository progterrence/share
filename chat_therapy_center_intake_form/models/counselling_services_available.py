from odoo import models, fields


class CounsellingServicesAvailable(models.Model):
    _name = 'counselling.services.available'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    share_service_number = fields.Char('Share Service Number')
    service_name = fields.Char('Service Name')
