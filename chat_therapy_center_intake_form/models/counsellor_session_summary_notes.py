from odoo import models, fields
from datetime import datetime, timedelta


class CounsellorSessionNotes(models.Model):
    _name = 'counsellor.session.summary.notes'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    counsellor_name = fields.Char('')
    counsellor_email = fields.Char('Email')
    session_date_id = fields.Date('Session Date')
    session_time = fields.Datetime('Session Time')
    client_name = fields.Char('Client Name')
    Session_no = fields.Char('Session #')
    subjective_summary = fields.Text('')
    objective_findings = fields.Text('')
    progress_assessments = fields.Text('')
    next_session_plans = fields.Text('')
